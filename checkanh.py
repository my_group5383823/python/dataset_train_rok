from PIL import Image
import numpy as np
from keras.models import load_model
from keras.preprocessing.image import img_to_array
import matplotlib.pyplot as plt

# Đường dẫn tới model
model_path = 'my_model.h5'
model = load_model(model_path)

# Đường dẫn tới ảnh đầu vào
image_path = 'doiten (1004)_0.png'

# Hàm tiền xử lý ảnh
def preprocess_image(image):
    image = image.resize((6, 13))  # Resize ảnh về kích thước phù hợp với model
    image = np.array(image)
    image = np.expand_dims(image, axis=0)  # Thêm batch dimension
    image = image.astype('float32') / 255  # Chuẩn hóa giá trị pixel
    return image

# Đọc ảnh đầu vào
original_image = Image.open(image_path) 

# Tiền xử lý ảnh
processed_image = preprocess_image(original_image)

# Dự đoán số trên ảnh
prediction = model.predict(processed_image)
predicted_number = np.argmax(prediction)

print(f"Kết quả nhận dạng số: {predicted_number}")

# Hiển thị ảnh đã được tiền xử lý
plt.imshow(processed_image[0]) # Loại bỏ batch dimension và đặt cmap để hiển thị ảnh grayscale
plt.title(f'Predicted: {predicted_number}')
plt.axis('off')
plt.show()
