from matplotlib import pyplot as plt
import numpy as np
from tensorflow.keras.models import load_model
from sklearn.metrics import accuracy_score, confusion_matrix

# Bước 1: Tải mô hình đã lưu
model = load_model('my_model.h5')

# Bước 2: Tải dữ liệu kiểm tra từ tệp .npz
data = np.load('test_data.npz')
x_test = data['x_test']
y_test = data['y_test']

# Kiểm tra xem y_test có phải là vector một-hot hay không
if len(y_test.shape) == 1:
    y_true = y_test
else:
    y_true = np.argmax(y_test, axis=1)

# Bước 3: Dự đoán kết quả sử dụng mô hình đã tải và dữ liệu kiểm tra
y_pred_prob = model.predict(x_test)
y_pred = np.argmax(y_pred_prob, axis=1)

# Bước 4: Đánh giá hiệu suất của mô hình
accuracy = accuracy_score(y_true, y_pred)
conf_matrix = confusion_matrix(y_true, y_pred)

plt.imshow(x_test[10]) # Loại bỏ batch dimension và đặt cmap để hiển thị ảnh grayscale
plt.title(f'Predicted: {"predicted_number"}')
plt.axis('off')
plt.show()


print(f'Accuracy: {accuracy * 100:.2f}%')
print('Confusion Matrix:')
print(conf_matrix)
