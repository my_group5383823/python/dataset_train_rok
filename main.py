import os
import numpy as np
from PIL import Image  #phai dùng thư viện này để đọc ảnh nếu không sẽ lỗi màu 
from sklearn.model_selection import train_test_split 
import pickle

import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input,Dropout, Conv2D, MaxPooling2D, Flatten, Dense,     BatchNormalization
from tensorflow.keras.optimizers import Adam

def load_images_from_folder(folder):
    images = []
    labels = []
    for class_name in os.listdir(folder):
        class_folder = os.path.join(folder, class_name)
        if os.path.isdir(class_folder):
            for filename in os.listdir(class_folder):
                img_path = os.path.join(class_folder, filename)
                img = Image.open(img_path) # Chuyển sang ảnh grayscale
                img = img.resize((6, 13))  # Thay đổi kích thước ảnh (tuỳ chỉnh kích thước)
                img_array = np.array(img)
                images.append(img_array)
                labels.append(int(class_name))  # Chuyển đổi nhãn thành số nguyên
    return np.array(images), np.array(labels)

# Tải dữ liệu huấn luyện và kiểm tra
train_folder = 'train'
test_folder = 'test'

x_train, y_train = load_images_from_folder(train_folder)
x_test, y_test = load_images_from_folder(test_folder)

# Hiển thị hình ảnh cùng với nhãn dự đoán và nhãn thực tế
plt.imshow(x_train[120])
plt.title(f'Predicted: {"predicted_label"}, True: {y_train[120]}')
plt.axis('off')
plt.show()

# Kiểm tra hình dạng của dữ liệu
print(f"x_train shape: {x_train.shape}")
print(f"y_train shape: {y_train.shape}")
print(f"x_test shape: {x_test.shape}")
print(f"y_test shape: {y_test.shape}")

# Chuẩn hóa dữ liệu ảnh (tuỳ chọn)
x_train = x_train.astype('float32') / 255.0
x_test = x_test.astype('float32') / 255.0

# Thêm một chiều kênh (cho các mô hình CNN)
# x_train = np.expand_dims(x_train, axis=-1)
# x_test = np.expand_dims(x_test, axis=-1)

print("Training data shape: ", x_train.shape)
print("Test data shape: ", x_test.shape)

index_to_display = 120  #10 Ví dụ: in ảnh đầu tiên

# Lưu trữ dữ liệu huấn luyện vào tệp 'train_data.npz'
np.savez('train_data.npz', x_train=x_train, y_train=y_train)

# Lưu trữ dữ liệu kiểm tra vào tệp 'test_data.npz'
np.savez('test_data.npz', x_test=x_test, y_test=y_test)

# Kiểm tra giá trị min và max
print(f'Min value in x_train: {x_train.min()}, Max value in x_train: {x_train.max()}')
print(f'Min value in x_test: {x_test.min()}, Max value in x_test: {x_test.max()}')

# Định nghĩa mô hình và huấn luyện
input_shape = (7, 13, 3)   # Đầu vào có 1 kênh cho ảnh grayscale

model = Sequential([
      Input(shape=input_shape),
    Conv2D(8, kernel_size=(3, 3), activation='relu', padding='same'),  # Giảm số lượng filter xuống còn 8
    MaxPooling2D(pool_size=(2, 2)),
    Flatten(),
    Dense(32, activation='relu'),  # Giảm số lượng neurons xuống còn 32
    Dropout(0.5),
    Dense(11, activation='softmax')  # Lớp đầu ra

])

model.compile(optimizer=Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.summary()

# Chuyển đổi thành vector 1 chiều
y_train_vector = np.array(y_train)
y_test_vector = np.array(y_test)



# In ra kích thước của vector mới
print("Kích thước của vector mới:", y_train_vector.shape)
model.fit(x_train, y_train_vector, validation_data =(x_test, y_test), epochs=150)
# Lưu mô hình
model.save("my_model.h5")


# Đánh giá mô hình
test_loss, test_accuracy = model.evaluate(x_test, y_test)

print("Test Loss:", test_loss)
print("Test Accuracy:", test_accuracy)


# Chọn một 98 từ dữ liệu kiểm tra
index = index_to_display   # bạn có thể thay đổi chỉ số này để chọn mẫu khác
x_sample = x_train[index]
y_true = y_train[index]

# Dự đoán trên mẫu đã chọn
x_sample = np.expand_dims(x_sample, axis=0)  # Thêm chiều batch
prediction = model.predict(x_sample)
predicted_label = np.argmax(prediction)



# Hiển thị hình ảnh cùng với nhãn dự đoán và nhãn thực tế
plt.imshow(x_train[index])
plt.title(f'Predicted: {predicted_label}, True: {y_true}')
plt.axis('off')
plt.show()

